import {composeWithDevTools} from "redux-devtools-extension";
import {applyMiddleware, createStore} from "redux";
import rootReducer from "./reducers";

const composedEnhancer = composeWithDevTools(
    applyMiddleware()
)

const store = createStore(rootReducer, composedEnhancer);

export default store;

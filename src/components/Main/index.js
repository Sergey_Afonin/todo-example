import React from "react";
import Container from "@material-ui/core/Container";
import Todo from "../Todo";

const Main = () => {

    return (
        <main>
            <Container maxWidth="sm">
                <Todo/>
            </Container>
        </main>
    )
}

export default Main;
import TodoInput from "./TodoInput";
import React from "react";
import TodoList from "./TodoList";
import {makeStyles} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import {ADD_TODO, DELETE_TODO} from "../../constants";

const useStyles = makeStyles({
    root: {
        padding: '20px 0'
    },
    input: {
        marginBottom: 20
    }
});

const selectTodos = store => store.todos;

const Todo = () => {
    const classes = useStyles();
    const todos = useSelector(selectTodos);
    const dispatch = useDispatch();

    const handleAddTodo = (text) => {
        dispatch({
            type: ADD_TODO,
            payload: text
        })
    }

    const handleDelete = (id) => {
        dispatch({
            type: DELETE_TODO,
            payload: id
        })
    }

    return (
        <div className={`${classes.root} todo`}>
            <TodoInput className={classes.input} onSubmit={handleAddTodo}/>
            <TodoList todos={todos} onDelete={handleDelete}/>
        </div>
    )
}

export default Todo;
import React from "react";
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles({
    root: {
      marginBottom: 10
    },
    paper: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5,
        paddingLeft: 10
    },
    text: {
        flex: 1,
        overflow: 'hidden',
        textOverflow: 'ellipsis'
    },
    divider: {
        margin: '5px 0',
        alignSelf: 'stretch',
        height: 'auto'
    }
})

const TodoItem = ({todo, onDelete}) => {
    const classes = useStyles();

    const handleDelete = () => {
        onDelete(todo.id)
    }

    return (
        <div className={`${classes.root} todo__list-item`}>
            <Paper className={classes.paper}>
                <span className={classes.text}>{todo.text}</span>
                <Divider className={classes.divider} orientation="vertical" />
                <IconButton onClick={handleDelete}>
                    <DeleteIcon/>
                </IconButton>
            </Paper>
        </div>
    )
}

export default TodoItem;
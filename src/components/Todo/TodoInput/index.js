import React, {useState} from "react";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from '@material-ui/icons/Add';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%'
    },
    inputBase: {
        padding: '5px 10px',
        flex: 1
    },
    divider: {
        margin: '5px 0',
        alignSelf: 'stretch',
        height: 'auto'
    }
}));

const TodoInput = ({className, onSubmit}) => {
    const classes = useStyles();

    const [inputValue, setInputValue] = useState('');

    const handleInputChange = (e) => {
        if(e.target.value){
            setInputValue(e.target.value);
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if(inputValue.trim().length) {
            onSubmit(inputValue.trim());
            setInputValue('');
        }
    }

    return (
        <div className={`todo__input ${className}`}>
            <Paper component="form"
                   className={classes.root}
                   onSubmit={handleSubmit}
                   elevation={3}>
                <InputBase
                    className={classes.inputBase}
                    onChange={handleInputChange}
                    value={inputValue}
                    placeholder="Enter text" />
                <Divider className={classes.divider} orientation="vertical" />
                <IconButton type={'submit'}>
                    <AddIcon/>
                </IconButton>
            </Paper>
        </div>
    )
}

export default TodoInput;
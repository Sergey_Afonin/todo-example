import {ADD_TODO, DELETE_TODO} from "../../constants";

const initialState = [];

const todosReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TODO:
            return [
                ...state,
                {
                    id: state.length ? state[state.length - 1].id + 1 : 1,
                    text: action.payload
                }
            ]
        case DELETE_TODO:
            return state.filter(item => item.id !== action.payload);

        default:
            return state;
    }
}

export default todosReducer;